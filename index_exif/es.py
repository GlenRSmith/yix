from json import dumps, loads
import os

import requests

try:
    ES_SERVER = os.environ['YIX_ES_SERVER']
except:
    ES_SERVER = "http://localhost:9200"
ES_TYPE = "exif_doc"
ES_INDEX = "exif_index"


class BulkIndexer(object):

    def __init__(self):
        self._data = ""

    def add_item(self, data):
        command_line_dict = {"create": {
            "_index": ES_INDEX, "_type": ES_TYPE, "_id": data["id"]}}
        self._data += \
            dumps(command_line_dict) + "\n" + dumps(data) + "\n"
        return

    def index(self):
        url = os.path.join(ES_SERVER, ES_INDEX, ES_TYPE, '_bulk')
        return requests.post(url, data=self._data)


def _check_return(request, expected_status):
    try:
        assert(request.status_code in expected_status)
    except AssertionError, e:
        print e
        print "expected_status: {0}".format(expected_status)
        print "status: {0}".format(request.status_code)
        print "content: {0}".format(request.content)
        raise e


def server_running():
    try:
        result = requests.get(ES_SERVER).json()
        return "ok" in result and result["ok"]
    except Exception, e:
        return False


def _flush(refresh=False):
    requests.post(os.path.join(ES_SERVER, ES_INDEX, '_flush'))


def get_doc_url(doc_id=None):
    if doc_id:
        doc_id = doc_id.replace(".", "_")
        return os.path.join(ES_SERVER, ES_INDEX, ES_TYPE, doc_id)
    else:
        return os.path.join(ES_SERVER, ES_INDEX, ES_TYPE)


def create_index(index_name=ES_INDEX):
    url = os.path.join(ES_SERVER, ES_INDEX)
    r = requests.put(url)
    _check_return(r, [200])
    return


def delete_index(index_name=ES_INDEX):
    url = ES_SERVER + index_name
    r = requests.delete(url)
    _check_return(r, [200])
    return


def index_doc(json_document, doc_id=None):
    url = get_doc_url(doc_id)
    print url
    if doc_id:
        r = requests.put(url, data=json_document)
    else:
        r = requests.post(url, data=json_document)
    print("results: {0}".format(r))
    print("results: {0}".format(r.text))
    _flush()
    return r


def submit_query(query_data, type_name):
    es_url = ES_SERVER + ES_INDEX + '/' + type_name + '/' + '_search'
    es_req = requests.get(es_url, data=query_data)
    _check_return(es_req, [200, 201])
    try:
        print 'es_req: {0}'.format(es_req)
        return es_req.json()
    except Exception, e:
        print 'es_query exception: {0}'.format(e)
        return loads(es_req.text)


def save_query(query_data, save_name):
    es_url = ES_SERVER + "_percolator/" + ES_INDEX + "/" + save_name
    r = requests.put(es_url, data=query_data)
    _check_return(r, [200, 201])
    _flush()
    return


def delete_query(save_name):
    es_url = ES_SERVER + "_percolator/" + ES_INDEX + "/" + save_name
    r = requests.delete(es_url)
    _check_return(r, [200])
    return


def add_map(es_type, map_data):
    es_url = ES_SERVER + ES_INDEX + '/' + es_type + '/_mapping'
    r = requests.put(es_url, data=map_data)
    _check_return(r, [200])
    return


def percolate(es_type, document):
    url = ES_SERVER + ES_INDEX + '/' + es_type + '/_percolate'
    data = {"doc": document}
    r = requests.get(url, data=dumps(data))
    try:
        result = r.json()["matches"]
    except Exception, e:
        print e
        result = e
    return result
