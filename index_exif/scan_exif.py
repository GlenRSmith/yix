#!/usr/bin/env python
# python built-ins
import argparse
import json
import os
import timeit

# third party
from PIL import Image
from PIL.ExifTags import TAGS

# in-project
import es

BASE_DIR = os.path.abspath(os.path.curdir)
DATA_DIR = os.path.join(BASE_DIR, 'images')

BULK_SIZE = 100


def paginate(long_list, entries):
    return [long_list[i:i + entries] for i in range(0, len(long_list), entries)]


def get_exif(fn):
    ret = {}
    i = Image.open(fn)
    if hasattr(i, '_getexif'):
        info = i._getexif()
        if info and hasattr(i, '_getexif'):
            for tag, value in info.items():
                decoded = TAGS.get(tag, tag)
                ret[decoded] = value
            return ret
        else:
            pass
            # print("{0} has no exif info".format(fn))
    else:
        pass
        # print("{0} has no getexif function".format(fn))


def scan_image(image_file):
    try:
        tags = get_exif(image_file)
        for k in tags:
            try:
                json.dumps({k:tags[k]})
            except Exception, e:
                tmp = tags[k].decode('latin-1')
                tags[k] = tmp.encode('utf-8')
    except Exception, e:
        print("scan_image exception: {0}".format(e))
        tags = None
    return tags


def index_image(fn):
    """
    Index the exif data in the file fn
    fn is the full path to a local file
    """
    # get the exif tags from the file
    tags = scan_image(fn)
    # get the bare filename to use as the indexed ID
    head, tail = os.path.split(fn)
    # call the elasticsearch interface to index
    es.index_doc(json.dumps(tags), tail)
    return tags


def bulk_index_list(image_files):
    bix = es.BulkIndexer()
    for image in image_files:
        tags = scan_image(image)
        head, tail = os.path.split(image)
        tags['id'] = tail
        bix.add_item(tags)
    bix.index()
    return


def scan_folder(index, dir_path=DATA_DIR):
    print("data_path: {0}".format(dir_path))
    tag_dict = {}
    files = os.listdir(dir_path)
    for name in files:
        fh = os.path.join(dir_path, name)
        try:
            if index:
                tag_dict[name] = index_image(fh)
            else:
                tag_dict[name] = scan_image(fh)
        except Exception, e:
            print "error parsing file {0}: {1}".format(name, e)
    print tag_dict.keys()
    for name in tag_dict:
        try:
            print("{0} has tags {1}\n".format(name,
                json.dumps(tag_dict[name].keys())))
        except Exception, e:
            print("{0} has no tags".format(name))
    return tag_dict


def bulk_index_images(dir_path=DATA_DIR, per_page_limit=BULK_SIZE):
    start = timeit.time.time()
    image_count = 0
    # scan in the filenames
    print("dirpath: {0}".format(dir_path))
    files = os.listdir(dir_path)
    full_files = []
    for name in files:
        full_files.append(os.path.join(dir_path, name))
    file_pages = paginate(full_files, per_page_limit)
    for page in file_pages:
        image_count += len(page)
        bulk_index_list(page)
    end = timeit.time.time()
    duration = end - start
    mean = round(1000.0 * duration / image_count, 2)
    print 'Processed {0} images in {1} seconds, average {2} ms/image'.format(
        image_count, duration, mean)
    return


def run(index, path_arg, bulk):
    if index or bulk:
        try:
            es.create_index()
        except:
            pass
    if not os.path.exists(path_arg):
        print("path {0} does not exist".format(path_arg))
        return
    if os.path.isdir(path_arg):
        if bulk:
            return bulk_index_images(path_arg)
        else:
            return scan_folder(index, path_arg)
    if os.path.isfile(path_arg):
        return index_image(path_arg)
    print("{0} is not a directory or a file".format(path_arg))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(add_help=True,
        description="Program to read, report, and index EXIF data from image files")
    parser.add_argument('-i', '--index', action='store_true',
                        help='index the image(s) that get scanned')
    parser.add_argument('-b', '--bulk', action='store_true',
                        help='use the bulk indexer if possible - obviates the -i argument')
    parser.add_argument('-p', '--image_path', default=DATA_DIR,
                        help='provide a folder or image to scan - default is "./images" folder')
    args = parser.parse_args()
    run(args.index, args.image_path, args.bulk)
