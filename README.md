yix: Your Image indeXer

A utility for scanning image files for exif data and indexing them in elasticsearch

With elasticsearch installed - from es_utils:
./serve_es.sh

From index_exif:
./scan_exif -h